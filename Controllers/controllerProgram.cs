﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoller
{
    public class EmployeeController : Controller
    {



        Hrdb dbobj = new Hrdb();
        public ActionResult Index()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            dbobj.Employees.Add(employee);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }


        public ActionResult AfterCreate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["User_id"]);
            string firname = entireFormData["FirstName"].ToString();
            string lastname = entireFormData["LastName"].ToString();
            string email = entireFormData["Email"].ToString();
            string passcode = entireFormData["Passcode"].ToString();
            string Address= entireFormData["Address"].ToString();
            string current_job_loc = entireFormData["job_loc"].ToString();
            double tenth_marks = Convert.ToInt32(entireFormData["t_marks"]);
            double twelth_marks = Convert.ToInt32(entireFormData["twelth_marks"]);

            Employee employee = new Employee() { EmpNo = id, Name = firname, Email = email, Passcode = passcode,  Address = Address, current_job_loc = current_job_loc,tenth_marks = tenth_marks,twelth_marks = twelth_marks};

            dbobj.Employees.Add(employee);

            dbobj.SaveChanges();

            return Redirect("/Employee/ListEmployee");
        }

        public ActionResult Update(int id)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            return View(employeeToBeUpdated);
        }


        public ActionResult AfterUpdate(Employee empUpdated)
        {
            var employeeToBeUpdated = (from Employee in dbobj.Employees
                                       where Employee.EmpNo == empUpdated.EmpNo
                                       select Employee).First();
            employeeToBeUpdated.Name = empUpdated.Name;
            employeeToBeUpdated.Email = empUpdated.Email;
            employeeToBeUpdated.Passcode = empUpdated.Passcode;
            employeeToBeUpdated.Salary = empUpdated.Salary;
            employeeToBeUpdated.Commision = empUpdated.Commision;
            employeeToBeUpdated.Mobileno = empUpdated.Mobileno;
            employeeToBeUpdated.Designation = empUpdated.Designation;
            employeeToBeUpdated.HireDate = empUpdated.HireDate;
            employeeToBeUpdated.DeptNo = empUpdated.DeptNo;
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");

        }

        public ActionResult Delete(int id)
        {
            var employeeToBeDeleted = (from Employee in dbobj.Employees where Employee.EmpNo == id select Employee).First();

            dbobj.Employees.Remove(employeeToBeDeleted);
            dbobj.SaveChanges();
            return Redirect("/Employee/ListEmployee");
        }


        public ActionResult ListEmployee()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }

        public ActionResult Display()
        {
            var employees = dbobj.Employees.ToList();
            return View(employees);
        }
    }
}